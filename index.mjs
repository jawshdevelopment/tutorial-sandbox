import express from 'express'
import bodyParser from 'body-parser'
import axios from 'axios'
import fs from 'fs'

fs.writeFileSync('data/myposts.json', '[]')

const app = express()
const boobs = `{o}{o}`

app.use(bodyParser.json())

app.get('/getmydata', async (req, res, next) => {
  try {
    const posts = await axios.get('https://jsonplaceholder.typicode.com/posts')
    res.status(200).send(posts)
  } catch (err) {
    next(err)
  }
})

app.post('/savemydata', (req, res, next) => {
  try {
    const myDataArray = JSON.parse(fs.readFileSync('data/myposts.json'))
    myDataArray.push(req.body)
    fs.writeFileSync('data/myposts.json', JSON.stringify(myDataArray))
    res.status(201).send(myDataArray)
  } catch (err) {
    next(err)
  }
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.listen(3000, () => {
  console.log('server listening on port 3000 🚀🚀🚀')
})
